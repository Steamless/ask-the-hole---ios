//
//  ViewController.swift
//  AskTheHole
//
//  Created by 98it006288mb on 05.01.20.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var holeEmpty: UIImageView!
    
    @IBAction func askButton(_ sender: Any) {
        print("Ask button pressed")
        
        let answersArray = [ #imageLiteral(resourceName: "askquestions1"),#imageLiteral(resourceName: "askquestions2"),#imageLiteral(resourceName: "askquestions5"),#imageLiteral(resourceName: "askquestions8"),#imageLiteral(resourceName: "askquestions4"),#imageLiteral(resourceName: "askquestions6"),#imageLiteral(resourceName: "askquestions3"),#imageLiteral(resourceName: "askquestions7") ]
        
        holeEmpty.image = answersArray.randomElement()
        
    }
    
}

